package org.firstinspires.ftc.teamcode

import com.qualcomm.robotcore.eventloop.opmode.TeleOp
import org.firstinspires.ftc.teamcode.hardware.Hardware

@TeleOp
class FoundationServosPosition: OpMode() {
    override fun Hardware.run() {
        var posLeft = 0.0
        var posRight = 0.0

        val gp1 = Gamepad(gamepad1)
        while(!isStarted){
            telemetry.addLine("Waiting for start...")
            telemetry.update()
            idle()
        }

        waitForStart()

        while(opModeIsActive()) {
            if(gp1.checkToggle(Gamepad.Button.A)) posLeft += 0.01
            if(gp1.checkToggle(Gamepad.Button.B)) posLeft -= 0.01
            if(gp1.checkToggle(Gamepad.Button.X)) posRight += 0.01
            if(gp1.checkToggle(Gamepad.Button.Y)) posRight -= 0.01
            foundation.setPosition(posLeft, posRight)
            telemetry.addData("PosLeft", posLeft)
            telemetry.addData("PosRight", posRight)
            telemetry.update()
        }
    }
}