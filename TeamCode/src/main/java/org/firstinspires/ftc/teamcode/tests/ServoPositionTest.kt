package org.firstinspires.ftc.teamcode.tests

import com.acmerobotics.dashboard.config.Config
import com.qualcomm.robotcore.eventloop.opmode.Autonomous
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode

@Autonomous
@Config
class ServoPositionTest : LinearOpMode() {
    companion object {
        @JvmField var position = 0.5
    }
    override fun runOpMode() {
        val servo = hardwareMap.servo.get("servo")
        waitForStart()
        while(opModeIsActive()) {
            servo.position = position

            telemetry.addData("Position", position)
            telemetry.update()
        }
    }
}