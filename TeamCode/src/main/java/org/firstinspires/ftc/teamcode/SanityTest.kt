package org.firstinspires.ftc.teamcode

import com.qualcomm.robotcore.eventloop.opmode.Disabled
import com.qualcomm.robotcore.eventloop.opmode.TeleOp
import com.qualcomm.robotcore.util.ElapsedTime
import org.firstinspires.ftc.teamcode.hardware.Hardware
import org.firstinspires.ftc.teamcode.hardware.Intake
import java.lang.Math.atan2
import kotlin.math.absoluteValue

@TeleOp(name = "SanityTest", group = "Main")
class SanityTest: OpMode() {
    override fun Hardware.run() {
        telemetry.addLine("pula mea")
        telemetry.update()
        while(!isStarted && !isStopRequested) {
            telemetry.addLine("Waiting for start....")
            telemetry.update()
            idle()
        }
        waitForStart()
    }

    /// The direction in which the robot is translating.
    private val direction: Double
        get() {
            val x = gamepad1.left_stick_x.toDouble()
            val y = -gamepad1.left_stick_y.toDouble()

            return atan2(y, x) / Math.PI * 180.0 - 90.0
        }

    /// Rotation around the robot's Z axis.
    private val rotation: Double
        get() = -gamepad1.right_stick_x.toDouble()

    /// Translation speed.
    private val speed: Double
        get() {
            val x = gamepad1.left_stick_x.toDouble()
            val y = gamepad1.left_stick_y.toDouble()

            return Math.sqrt((x * x) + (y * y))
        }
}
