package org.firstinspires.ftc.teamcode.autonomy

import android.view.VelocityTracker
import com.acmerobotics.dashboard.config.Config
import com.acmerobotics.roadrunner.geometry.Pose2d
import com.acmerobotics.roadrunner.geometry.Vector2d
import com.acmerobotics.roadrunner.path.heading.HeadingInterpolator
import com.qualcomm.robotcore.eventloop.opmode.Autonomous
import org.firstinspires.ftc.teamcode.OpMode
import org.firstinspires.ftc.teamcode.RoadRunner.drive.mecanum.SampleMecanumDriveREVOptimized
import org.firstinspires.ftc.teamcode.hardware.Hardware
import org.firstinspires.ftc.teamcode.waitMillis
import java.util.*

@Autonomous
@Config
class AutonomyRedFoundation : OpMode() {
    val drive by lazy {
        SampleMecanumDriveREVOptimized(hardwareMap)
    }

    /// 1 tile = 22.75
    public companion object{
        @JvmField var dist1 = 2.1
        @JvmField var dist2 = 33.1
        @JvmField var dist3 = 20.1
        @JvmField var dist4 = 40.1
        @JvmField var dist5 = 25.1
        @JvmField var dist6 = 20.1

        @JvmField var dist7 = 20.1
        @JvmField var angle = 125.0
    }

    override fun Hardware.run() {
        drive.followTrajectorySync(drive.trajectoryBuilder().strafeRight(dist1).back(dist2).build())

        foundation.grab(true)
        waitMillis(500)

        drive.followTrajectorySync(drive.trajectoryBuilder().strafeLeft(dist7).build())
        drive.followTrajectorySync(drive.trajectoryBuilder().forward(dist3).build())

        drive.turnSync(Math.toRadians(90.0))

        drive.followTrajectorySync(drive.trajectoryBuilder().back(dist4).build())

        foundation.grab(false)

        waitMillis(500)
        drive.followTrajectorySync(drive.trajectoryBuilder().forward(5.0).build())
        drive.turnSync(Math.toRadians(180.0))

//        drive.followTrajectorySync(drive.trajectoryBuilder().strafeLeft(dist5).back(dist6).build())
    }
}