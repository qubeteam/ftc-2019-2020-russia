package org.firstinspires.ftc.teamcode.tests

import com.qualcomm.robotcore.eventloop.opmode.Autonomous
import org.firstinspires.ftc.teamcode.OpMode
import org.firstinspires.ftc.teamcode.hardware.Hardware

@Autonomous
class ShowPositions : OpMode() {
    override fun Hardware.run() {

        waitForStart()

        while(opModeIsActive()) {
            intake.printPosition(telemetry)
            motors.printPosition(telemetry)
            outTake.printPosition(telemetry)
            telemetry.update()
        }
    }
}