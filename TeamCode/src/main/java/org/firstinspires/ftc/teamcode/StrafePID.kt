package org.firstinspires.ftc.teamcode

object StrafePID {
     var p : Double = 0.13
     var i : Double = 0.0
     var d : Double = 0.07
     var dist : Double = 10.3
     var ratio : Double = 7.7
}
