package org.firstinspires.ftc.teamcode.hardware.sensors

import com.qualcomm.robotcore.hardware.DistanceSensor
import com.qualcomm.robotcore.hardware.HardwareMap
import org.firstinspires.ftc.robotcore.external.navigation.DistanceUnit

class DistanceColorSensor(hwMap: HardwareMap) {
    val sensor = hwMap.get(DistanceSensor::class.java, "colorDistanceSensor")

    fun getDistanceCm() : Double {
        return sensor.getDistance(DistanceUnit.CM)
    }
}