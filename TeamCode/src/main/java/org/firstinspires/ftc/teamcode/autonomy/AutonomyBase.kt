package org.firstinspires.ftc.teamcode.autonomy

import org.firstinspires.ftc.robotcore.external.hardware.camera.WebcamName
import org.firstinspires.ftc.teamcode.OpMode
import org.firstinspires.ftc.teamcode.SkystonePosition
import org.opencv.core.Core
import org.opencv.core.Mat
import org.opencv.core.Point
import org.opencv.core.Scalar
import org.opencv.imgproc.Imgproc
import org.openftc.easyopencv.OpenCvCamera
import org.openftc.easyopencv.OpenCvCameraFactory
import org.openftc.easyopencv.OpenCvPipeline

abstract class AutonomyBase : OpMode() {
    var skystonePosition = SkystonePosition.MID
    val rectSize = 50
    val webcam: OpenCvCamera by lazy {
        val cameraMonitorViewId = hardwareMap.appContext.resources.getIdentifier("cameraMonitorViewId", "id", hardwareMap.appContext.packageName)
        OpenCvCameraFactory.getInstance().createWebcam(hardwareMap.get(WebcamName::class.java, "Webcam 1"), cameraMonitorViewId)
    }

    override fun preInit() {
        telemetry.addLine("Initializing")
        telemetry.update()
        webcam.openCameraDevice()

    }

    override fun preInitLoop() {

    }

    protected fun followTrajectory(steps: ArrayList<Pair<Double, Double>>){
        steps.forEach { follow(it) }
    }

    private fun follow(step: Pair<Double, Double>) {
        val (distance, heading) = step
        goTo(distance, heading)
    }
}
