package org.firstinspires.ftc.teamcode.hardware

import com.qualcomm.robotcore.hardware.*
import org.firstinspires.ftc.robotcore.external.Telemetry
import java.io.File
import java.io.FileOutputStream
import java.io.PrintWriter
import java.util.*
import kotlin.NoSuchElementException

/**
 * Intake motors and servos subsystem
 *
 * This class controls the hardware which takes the minerals
 */

class Foundation (hwMap: HardwareMap) {

    val leftServo = hwMap.servo.get("leftFoundationServo")
            ?: throw Exception("failed to find motor leftFoundationServo")
    val rightServo = hwMap.servo.get("rightFoundationServo")
            ?: throw Exception("failed to find motor rightFoundationServo")

    val leftServoOpen = 0.63
    val leftServoGrab = 0.35
    val rightServoOpen = 0.80
    val rightServoGrab = 0.53

    init {
        leftServo.position = leftServoOpen
        leftServo.direction = Servo.Direction.REVERSE
        rightServo.position = rightServoOpen
    }

    fun grab(af : Boolean) {
        if(af == true) {
            leftServo.position = leftServoGrab
            rightServo.position = rightServoGrab
        } else {
            leftServo.position = leftServoOpen
            rightServo.position = rightServoOpen
        }
    }
    fun stop() {
        leftServo.position = leftServoOpen
        rightServo.position = rightServoOpen
    }

    fun setPosition(posLeft: Double, posRight: Double) {
        leftServo.position = posLeft
        rightServo.position = posRight
    }
}
