package org.firstinspires.ftc.teamcode.autonomy;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;

import org.firstinspires.ftc.robotcore.external.hardware.camera.WebcamName;
import org.firstinspires.ftc.teamcode.OpMode;
import org.firstinspires.ftc.teamcode.SkystonePosition;
import org.firstinspires.ftc.teamcode.hardware.Hardware;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;
import org.openftc.easyopencv.OpenCvCamera;
import org.openftc.easyopencv.OpenCvCameraFactory;
import org.openftc.easyopencv.OpenCvCameraRotation;
import org.openftc.easyopencv.OpenCvPipeline;

@TeleOp
public class AutonomyBlue extends LinearOpMode{
    OpenCvCamera webcam;
    int Y, leftX, midX, rightX, rectSize;
    SkystonePosition pos;
    @Override
    public void runOpMode() {
        int cameraMonitorViewId = hardwareMap.appContext.getResources().getIdentifier("cameraMonitorViewId", "id", hardwareMap.appContext.getPackageName());
        webcam = OpenCvCameraFactory.getInstance().createWebcam(hardwareMap.get(WebcamName.class, "Webcam 1"), cameraMonitorViewId);
        webcam.openCameraDevice();
        webcam.setPipeline(new SkystonePipeline());
        webcam.startStreaming(320, 240, OpenCvCameraRotation.UPRIGHT);

        waitForStart();

        while (opModeIsActive())
        {
            /*
             * Send some stats to the telemetry
             */
            telemetry.addData("Frame Count", webcam.getFrameCount());
            telemetry.addData("FPS", String.format("%.2f", webcam.getFps()));
            telemetry.addData("Total frame time ms", webcam.getTotalFrameTimeMs());
            telemetry.addData("Pipeline time ms", webcam.getPipelineTimeMs());
            telemetry.addData("Overhead time ms", webcam.getOverheadTimeMs());
            telemetry.addData("Theoretical max FPS", webcam.getCurrentPipelineMaxFps());
            telemetry.update();

            /*
             * NOTE: stopping the stream from the camera early (before the end of the OpMode
             * when it will be automatically stopped for you) *IS* supported. The "if" statement
             * below will stop streaming from the camera when the "A" button on gamepad 1 is pressed.
             */
            if(gamepad1.a)
            {
                /*
                 * IMPORTANT NOTE: calling stopStreaming() will indeed stop the stream of images
                 * from the camera (and, by extension, stop calling your vision pipeline). HOWEVER,
                 * if the reason you wish to stop the stream early is to switch use of the camera
                 * over to, say, Vuforia or TFOD, you will also need to call closeCameraDevice()
                 * (commented out below), because according to the Android Camera API documentation:
                 *         "Your application should only have one Camera object active at a time for
                 *          a particular hardware camera."
                 *
                 * NB: calling closeCameraDevice() will internally call stopStreaming() if applicable,
                 * but it doesn't hurt to call it anyway, if for no other reason than clarity.
                 *
                 * NB2: if you are stopping the camera stream to simply save some processing power
                 * (or battery power) for a short while when you do not need your vision pipeline,
                 * it is recommended to NOT call closeCameraDevice() as you will then need to re-open
                 * it the next time you wish to activate your vision pipeline, which can take a bit of
                 * time. Of course, this comment is irrelevant in light of the use case described in
                 * the above "important note".
                 */
                webcam.stopStreaming();
                //webcam.closeCameraDevice();
            }

            /*
             * The viewport (if one was specified in the constructor) can also be dynamically "paused"
             * and "resumed". The primary use case of this is to reduce CPU, memory, and power load
             * when you need your vision pipeline running, but do not require a live preview on the
             * robot controller screen. For instance, this could be useful if you wish to see the live
             * camera preview as you are initializing your robot, but you no longer require the live
             * preview after you have finished your initialization process; pausing the viewport does
             * not stop running your pipeline.
             *
             * The "if" statements below will pause the viewport if the "X" button on gamepad1 is pressed,
             * and resume the viewport if the "Y" button on gamepad1 is pressed.
             */
            else if(gamepad1.x)
            {
                webcam.pauseViewport();
            }
            else if(gamepad1.y)
            {
                webcam.resumeViewport();
            }

            /*
             * For the purposes of this sample, throttle ourselves to 10Hz loop to avoid burning
             * excess CPU cycles for no reason. (By default, telemetry is only sent to the DS at 4Hz
             * anyway). Of course in a real OpMode you will likely not want to do this.
             */
            sleep(100);
        }
    }

    class SkystonePipeline extends OpenCvPipeline{
        Mat yCbCrChan2Mat = new Mat();
        Mat thresholdMat = new Mat();
        @Override
        public Mat processFrame(Mat input) {
            Imgproc.cvtColor(input, yCbCrChan2Mat, Imgproc.COLOR_RGB2YCrCb);
            Core.extractChannel(yCbCrChan2Mat, yCbCrChan2Mat, 2);
            Imgproc.threshold(yCbCrChan2Mat, thresholdMat, 102.0, 255.0, Imgproc.THRESH_BINARY_INV);
            double sumLeft = 0.0;
            double sumMid = 0.0;
            double sumRight = 0.0;
            for (int i = 0; i < rectSize; ++i) {
                for (int j = 0; j < rectSize; ++j) {
                    sumLeft += thresholdMat.get(leftX + i, Y + j)[0];
                }
            }
            for (int i = 0; i < rectSize; ++i) {
                for (int j = 0; j < rectSize; ++j) {
                    sumMid += thresholdMat.get(midX + i, Y + j)[0];
                }
            }
            for (int i = 0; i < rectSize; ++i) {
                for (int j = 0; j < rectSize; ++j) {
                    sumRight += thresholdMat.get(rightX + i, Y + j)[0];
                }
            }
            if (sumLeft > sumRight && sumLeft > sumMid) {
                pos = SkystonePosition.LEFT;
            } else if (sumMid > sumRight) {
                pos = SkystonePosition.MID;
            } else pos = SkystonePosition.RIGHT;
            Imgproc.rectangle(
                    input,
                    new Point(
                            leftX,
                            Y),
                    new Point(
                            leftX + rectSize,
                            Y + rectSize),
                    new Scalar(0.0, 255.0, 0.0), 4);
            Imgproc.rectangle(
                    input,
                    new Point(
                            midX,
                            Y),
                    new Point(
                            midX + rectSize,
                            Y + rectSize),
                    new Scalar(0.0, 255.0, 0.0), 4);
            Imgproc.rectangle(
                    input,
                    new Point(
                            rightX,
                            Y),
                    new Point(
                            rightX + rectSize,
                            Y + rectSize),
                    new Scalar(0.0, 255.0, 0.0), 4);
            /**
             * NOTE: to see how to get data from your pipeline to your OpMode as well as how
             * to change which stage of the pipeline is rendered to the viewport when it is
             * tapped, please see [PipelineStageSwitchingExample]
             */
            return input;
        }
    }

}
