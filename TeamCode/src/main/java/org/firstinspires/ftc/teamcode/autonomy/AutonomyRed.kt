package org.firstinspires.ftc.teamcode.autonomy

import com.qualcomm.robotcore.eventloop.opmode.Autonomous
import org.firstinspires.ftc.robotcore.external.hardware.camera.WebcamName
import org.firstinspires.ftc.teamcode.OpMode
import org.firstinspires.ftc.teamcode.RoadRunner.drive.mecanum.SampleMecanumDriveBase
import org.firstinspires.ftc.teamcode.RoadRunner.drive.mecanum.SampleMecanumDriveREVOptimized
import org.firstinspires.ftc.teamcode.SkystonePosition
import org.firstinspires.ftc.teamcode.hardware.Hardware
import org.firstinspires.ftc.teamcode.waitMillis
import org.opencv.core.Core
import org.opencv.core.Mat
import org.opencv.core.Point
import org.opencv.core.Scalar
import org.opencv.imgproc.Imgproc
import org.openftc.easyopencv.OpenCvCamera
import org.openftc.easyopencv.OpenCvCameraFactory
import org.openftc.easyopencv.OpenCvCameraRotation
import org.openftc.easyopencv.OpenCvPipeline

@Autonomous
class AutonomyRed: OpMode() {

    var pos = SkystonePosition.MID

    val Y = 115
    val leftX = 0
    val midX = 75
    val rightX = 150
    val rectSize = 50

    val webcam: OpenCvCamera by lazy {
        val cameraMonitorViewId = hardwareMap.appContext.resources.getIdentifier("cameraMonitorViewId", "id", hardwareMap.appContext.packageName)
        OpenCvCameraFactory.getInstance().createWebcam(hardwareMap.get(WebcamName::class.java, "Webcam 1"), cameraMonitorViewId)
    }

    val drive: SampleMecanumDriveBase by lazy {
        SampleMecanumDriveREVOptimized(hardwareMap)
    }
    override fun preInit() {
        telemetry.addLine("Initializing...")
        telemetry.update()
        webcam.openCameraDevice()
        webcam.setPipeline(SkystonePipeline())
        webcam.startStreaming(320, 240, OpenCvCameraRotation.UPRIGHT)
    }

    override fun preInitLoop() {
        telemetry.addLine("Waiting for start...")
        telemetry.addData("Skystone position", pos)
        telemetry.update()
        idle()
    }

    override fun Hardware.run() {
    }


    inner class SkystonePipeline : OpenCvPipeline() {
        var yCbCrChan2Mat = Mat()
        var thresholdMat = Mat()
        override fun processFrame(input: Mat): Mat {
            Imgproc.cvtColor(input, yCbCrChan2Mat, Imgproc.COLOR_RGB2YCrCb)
            Core.extractChannel(yCbCrChan2Mat, yCbCrChan2Mat, 2)
            Imgproc.threshold(yCbCrChan2Mat, thresholdMat, 102.0, 255.0, Imgproc.THRESH_BINARY_INV)
            var sumLeft = 0.0
            var sumMid = 0.0
            var sumRight = 0.0
            for (i in 0 until rectSize) {
                for (j in 0 until rectSize) {
                    sumLeft += thresholdMat.get(leftX + i, Y + j).get(0)
                }
            }
            for (i in 0 until rectSize) {
                for (j in 0 until rectSize) {
                    sumMid += thresholdMat.get(midX + i, Y + j).get(0)
                }
            }
            for (i in 0 until rectSize) {
                for (j in 0 until rectSize) {
                    sumRight += thresholdMat.get(rightX + i, Y + j).get(0)
                }
            }
            if (sumLeft > sumRight && sumLeft > sumMid) {
                pos = SkystonePosition.LEFT
            } else if (sumMid > sumRight) {
                pos = SkystonePosition.MID
            } else pos = SkystonePosition.RIGHT

            Imgproc.rectangle(
                    input,
                    Point(
                            leftX.toDouble(),
                            Y.toDouble()),
                    Point(
                            leftX + rectSize.toDouble(),
                            Y + rectSize.toDouble()),
                    Scalar(0.0, 255.0, 0.0), 4)
            Imgproc.rectangle(
                    input,
                    Point(
                            midX.toDouble(),
                            Y.toDouble()),
                    Point(
                            midX + rectSize.toDouble(),
                            Y + rectSize.toDouble()),
                    Scalar(0.0, 255.0, 0.0), 4)
            Imgproc.rectangle(
                    input,
                    Point(
                            rightX.toDouble(),
                            Y.toDouble()),
                    Point(
                            rightX + rectSize.toDouble(),
                            Y + rectSize.toDouble()),
                    Scalar(0.0, 255.0, 0.0), 4)
            /**
             * NOTE: to see how to get data from your pipeline to your OpMode as well as how
             * to change which stage of the pipeline is rendered to the viewport when it is
             * tapped, please see [PipelineStageSwitchingExample]
             */
            return input
        }
    }
}