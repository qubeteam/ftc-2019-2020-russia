package ro.cnmv.qube.ftc.autonomy

import com.acmerobotics.dashboard.config.Config

data class CustomPair(@JvmField var angle: Double, @JvmField var distance: Double)

@Config
object Values {
    @JvmField var INITIAL_LEFT = CustomPair(110.0, 0.0)
    @JvmField var INITIAL_RIGHT = CustomPair(48.0, 0.0)
    @JvmField var INITIAL_CENTER = CustomPair(80.0, 0.0)
    @JvmField var TEAMMATE_LEFT = CustomPair(-60.0, 0.0)
    @JvmField var TEAMMATE_RIGHT = CustomPair(0.0, 0.0)
    @JvmField var TEAMMATE_CENTER = CustomPair(-25.0, 0.0)
    @JvmField var LAND_DISTANCE = -7.0
    @JvmField var LAND_STRAFE = 500
}
