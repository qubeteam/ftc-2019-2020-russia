package org.firstinspires.ftc.teamcode.tests

import com.qualcomm.robotcore.eventloop.opmode.TeleOp
import org.firstinspires.ftc.teamcode.OpMode
import org.firstinspires.ftc.teamcode.hardware.Hardware

@TeleOp
class OutTakeTest : OpMode() {
    override fun Hardware.run() {

        waitForStart()

        while(opModeIsActive()) {
            val power = (gamepad2.right_trigger - gamepad2.left_trigger).toDouble()
            outTake.setPowers(power, power)
        }
    }
}