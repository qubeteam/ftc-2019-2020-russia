package org.firstinspires.ftc.teamcode.hardware

import com.qualcomm.robotcore.hardware.*
import org.firstinspires.ftc.robotcore.external.Telemetry
import java.io.File
import java.io.FileOutputStream
import java.io.PrintWriter
import java.util.*
import kotlin.NoSuchElementException

/**
* Intake motors and servos subsystem
*
* This class controls the hardware which takes the minerals
 */

class Intake (hwMap: HardwareMap) {
    val leftIntakeMotor = hwMap.dcMotor.get("leftIntakeMotor")
            ?: throw Exception("failed to find motor leftIntakeMotor")
    val rightIntakeMotor = hwMap.dcMotor.get("rightIntakeMotor")
            ?: throw Exception("failed to find motor rightIntakeMotor")

    init {
        leftIntakeMotor.direction = DcMotorSimple.Direction.FORWARD
        rightIntakeMotor.direction = DcMotorSimple.Direction.REVERSE

        leftIntakeMotor.zeroPowerBehavior = DcMotor.ZeroPowerBehavior.BRAKE
        rightIntakeMotor.zeroPowerBehavior = DcMotor.ZeroPowerBehavior.BRAKE
        stopIntake();
    }

    /// Intake Motors
    //---------------------------------------------------------------------------------------------------------------------------
    fun setIntakePower(powerLeft: Double, powerRight: Double) {
        leftIntakeMotor.power = powerLeft
        rightIntakeMotor.power = powerRight
    }

    fun stopIntake() {
        setIntakePower(0.0, 0.0)
    }

    //---------------------------------------------------------------------------------------------------------------------------

    fun printPosition(telemetry: Telemetry) {
        telemetry.addLine("Position intake")
                .addData("Left", "%d", leftIntakeMotor.currentPosition)
                .addData("Right", "%d", rightIntakeMotor.currentPosition)
    }

    fun stop() {
        stopIntake()
    }

}
