package org.firstinspires.ftc.teamcode

object RotatePID {
     var p: Double = 0.8
     var i: Double = 0.1
     var d: Double = 1.5
     var angle: Double = 0.1
     var slow: Double = 0.1
     var threshold: Double = 0.1
}
