package org.firstinspires.ftc.teamcode.autonomy

import android.view.VelocityTracker
import com.acmerobotics.dashboard.config.Config
import com.acmerobotics.roadrunner.geometry.Pose2d
import com.acmerobotics.roadrunner.geometry.Vector2d
import com.acmerobotics.roadrunner.path.heading.HeadingInterpolator
import com.qualcomm.robotcore.eventloop.opmode.Autonomous
import org.firstinspires.ftc.teamcode.OpMode
import org.firstinspires.ftc.teamcode.RoadRunner.drive.mecanum.SampleMecanumDriveREVOptimized
import org.firstinspires.ftc.teamcode.hardware.Hardware
import org.firstinspires.ftc.teamcode.waitMillis
import java.util.*

@Autonomous
@Config
class AutonomyStrafeLeftGoBack : OpMode() {
    val drive by lazy {
        SampleMecanumDriveREVOptimized(hardwareMap)
    }

    override fun Hardware.run() {
        drive.followTrajectorySync(drive.trajectoryBuilder().strafeLeft(16.0).back(15.0).build())
    }
}