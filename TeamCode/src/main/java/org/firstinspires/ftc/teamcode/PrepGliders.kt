package org.firstinspires.ftc.teamcode

import com.acmerobotics.roadrunner.util.NanoClock
import com.qualcomm.robotcore.eventloop.opmode.TeleOp
import com.qualcomm.robotcore.util.ElapsedTime
import org.firstinspires.ftc.teamcode.hardware.Hardware
import kotlin.time.Clock

@TeleOp(name = "PrepGliders", group = "Preps")
class PrepGliders: OpMode() {

    val TRESH = 0
    val TIME_ALLOWED = 2000
    val POWER = 0.05

    override fun preInitLoop() {
        telemetry.addLine("Waiting for start...")
        telemetry.update()
        idle()
    }

    override fun Hardware.run() {
        var clock = ElapsedTime()
        var lastPosLeft = -100
        var lastPosRight = -100
        var timeLeft = 0
        var timeRight = 0

        var leftFinish = false
        var rightFinish = false

        outTake.setIndividualPower("left", POWER)
        outTake.setIndividualPower("right", POWER)
        while (opModeIsActive() && !isStopRequested){
            //Left
            if (outTake.leftOuttakeSlider.currentPosition - lastPosLeft > TRESH && !leftFinish) {
                telemetry.addLine("Calibrating Left")
                lastPosLeft = outTake.leftOuttakeSlider.currentPosition
                timeLeft = clock.milliseconds().toInt()
            }
            else{
                if (leftFinish || clock.milliseconds().toInt() - timeLeft > TIME_ALLOWED){
                    outTake.setIndividualPower("left", 0.0)
                    leftFinish = true
                    telemetry.addLine("Left is done!")
                }
                else{
                    telemetry.addLine("Left is finishing")
                }
            }

            //Right
            if (outTake.rightOuttakeSlider.currentPosition - lastPosRight > TRESH && !rightFinish) {
                telemetry.addLine("Calibrating Right")
                lastPosRight = outTake.rightOuttakeSlider.currentPosition
                timeRight = clock.milliseconds().toInt()
            }
            else{
                if (rightFinish || clock.milliseconds().toInt() - timeRight > TIME_ALLOWED){
                    outTake.setIndividualPower("right", 0.0)
                    rightFinish = true
                    telemetry.addLine("Right is done!")
                }
                else{
                    telemetry.addLine("Right is finishing")
                }
            }

            outTake.printPosition(telemetry)

            telemetry.update()

            //if (leftFinish && rightFinish) return

        }
    }
}