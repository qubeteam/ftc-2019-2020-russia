package org.firstinspires.ftc.teamcode.autonomy

import com.qualcomm.robotcore.eventloop.opmode.Autonomous
import org.firstinspires.ftc.teamcode.hardware.Hardware

@Autonomous(name = "Trajectory Tester", group = "Tests")
class TrajectoryTester : AutonomyBase() {
    override fun preInit() {
    }

    override fun Hardware.run() {
        while (!isStarted) {
        }

        waitForStart()

        followTrajectory(Trajectory.asArray())
    }
}
