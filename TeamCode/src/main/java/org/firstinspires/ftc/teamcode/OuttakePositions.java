package org.firstinspires.ftc.teamcode;

import com.acmerobotics.dashboard.config.Config;
import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;

import org.firstinspires.ftc.teamcode.hardware.Outtake;

@Autonomous
@Config
public class OuttakePositions extends LinearOpMode {
    public static double backLeft = Outtake.backLeftServoClosed;
    public static double backRight = Outtake.backRightServoClosed;
    public static double frontLeft = Outtake.frontLeftServoClosed;
    public static double frontRight = Outtake.frontRightServoClosed;
    public static double grab = Outtake.grabServoOpen;
    public static double cap = Outtake.capServoOpen;
    @Override
    public void runOpMode() {
        Outtake outake = new Outtake(hardwareMap);

        telemetry.addLine("Waiting for start...");
        telemetry.update();
        waitForStart();

        while(opModeIsActive()) {
            outake.setServoPositions(backLeft, backRight, frontLeft, frontRight, grab, cap);
            telemetry.addLine("Running...");
            telemetry.update();
        }

    }
}
