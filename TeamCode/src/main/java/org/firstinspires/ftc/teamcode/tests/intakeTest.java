package org.firstinspires.ftc.teamcode.tests;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;

import org.firstinspires.ftc.teamcode.hardware.Intake;

@TeleOp
public class intakeTest extends LinearOpMode {
    @Override
    public void runOpMode() {

        Intake intake = new Intake(hardwareMap);

        waitForStart();
        while(opModeIsActive()) {
            intake.setIntakePower((double)gamepad2.left_stick_y, (double)gamepad2.right_stick_y);
        }
    }
}
