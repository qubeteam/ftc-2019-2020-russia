package org.firstinspires.ftc.teamcode.hardware

import com.qualcomm.robotcore.hardware.DcMotor
import com.qualcomm.robotcore.hardware.DcMotorSimple
import com.qualcomm.robotcore.hardware.HardwareMap
import com.qualcomm.robotcore.hardware.Servo
import org.firstinspires.ftc.robotcore.external.Telemetry
import org.yaml.snakeyaml.tokens.FlowEntryToken
import java.io.File
import java.io.FileOutputStream
import java.io.PrintWriter
import java.util.*
import kotlin.math.absoluteValue

/**
 * OutTake subsystem.
 *
 * This class controls the hardware for placing stones
 */
class Outtake(hwMap: HardwareMap) {
    companion object {
        const val SLIDER_OPEN = 1850
        const val SLIDER_CLOSE = 0
        var SLIDER_START_POSITION = 0
        const val MULTIPLIER = 100

        const val backLeftServoOpen = 0.4
        const val backLeftServoClosed = 0.9
        const val backRightServoOpen = 0.65
        const val backRightServoClosed = 0.16
        const val frontLeftServoOpen = 0.86
        const val frontLeftServoClosed = 0.32
        const val frontRightServoOpen = 0.27
        const val frontRightServoClosed = 0.79
        const val grabServoClosed = 0.2
        const val grabServoOpen = 0.5

        const val backLeftServoDown = 0.29
        const val backRightServoDown = 0.78
        const val frontLeftServoDown = 0.98
        const val frontRightServoDown = 0.13
        const val grabServoCapstone = 1
        const val rotateServoStraight = 0.0
        const val rotateServoTurned = 0.0
        const val capServoOpen = 0.8
        const val capServoClose = 0.5
    }

    val leftOuttakeSlider = hwMap.dcMotor["leftOuttakeSlider"]
            ?: throw Exception("Failed to find motor leftOuttakeSlider")
    val rightOuttakeSlider = hwMap.dcMotor["rightOuttakeSlider"]
            ?: throw Exception("Failed to find motor rightOuttakeSlider")

    var outTakePosition: Int = 0

    val backLeftServo = hwMap.servo["backLeftServo"]
            ?:throw Exception("Failed to find servo backLeftServo")

    val backRightServo = hwMap.servo["backRightServo"]
            ?:throw Exception("Failed to find servo backRightServo")

    val frontLeftServo = hwMap.servo["frontLeftServo"]
            ?:throw Exception("Failed to find servo frontLeftServo")

    val frontRightServo = hwMap.servo["frontRightServo"]
            ?:throw Exception("Failed to find servo frontRightServo")

    val grabServo = hwMap.servo["grabServo"]
            ?:throw Exception("Failed to find servo grabServo")

    val capServo = hwMap.servo["servo"]
            ?:throw Exception("Failed to find servo capServo")

    init {
        leftOuttakeSlider.zeroPowerBehavior = DcMotor.ZeroPowerBehavior.FLOAT
        leftOuttakeSlider.direction = DcMotorSimple.Direction.FORWARD
        leftOuttakeSlider.mode = DcMotor.RunMode.STOP_AND_RESET_ENCODER
        leftOuttakeSlider.mode = DcMotor.RunMode.RUN_USING_ENCODER

        leftOuttakeSlider.power = 0.0


        rightOuttakeSlider.zeroPowerBehavior = DcMotor.ZeroPowerBehavior.FLOAT
        rightOuttakeSlider.direction = DcMotorSimple.Direction.REVERSE
        rightOuttakeSlider.mode = DcMotor.RunMode.STOP_AND_RESET_ENCODER
        rightOuttakeSlider.mode = DcMotor.RunMode.RUN_USING_ENCODER

        rightOuttakeSlider.power = 0.0

        outTakePosition = 0

        placeStone(false, false)
        grabServo.position = grabServoOpen

        capServo.position = capServoOpen
    }

    fun openSlider() {
        outTakePosition = SLIDER_OPEN
        leftOuttakeSlider.targetPosition = outTakePosition
        leftOuttakeSlider.mode = DcMotor.RunMode.RUN_TO_POSITION
        leftOuttakeSlider.power = 1.0

        rightOuttakeSlider.targetPosition = outTakePosition
        rightOuttakeSlider.mode = DcMotor.RunMode.RUN_TO_POSITION
        rightOuttakeSlider.power = 1.0
    }

    fun closeSlider() {
        outTakePosition = SLIDER_CLOSE

        outTakePosition = SLIDER_OPEN
        leftOuttakeSlider.targetPosition = outTakePosition
        leftOuttakeSlider.zeroPowerBehavior = DcMotor.ZeroPowerBehavior.FLOAT
        leftOuttakeSlider.mode = DcMotor.RunMode.RUN_TO_POSITION
        leftOuttakeSlider.power = 1.0

        rightOuttakeSlider.targetPosition = outTakePosition
        rightOuttakeSlider.zeroPowerBehavior = DcMotor.ZeroPowerBehavior.FLOAT
        rightOuttakeSlider.mode = DcMotor.RunMode.RUN_TO_POSITION
        rightOuttakeSlider.power = 1.0
    }

    fun moveSlider(power: Double) {
        outTakePosition += (power * MULTIPLIER).toInt()
        outTakePosition += SLIDER_START_POSITION

        if(outTakePosition > SLIDER_OPEN) outTakePosition = SLIDER_OPEN
        if(outTakePosition < SLIDER_CLOSE) outTakePosition = SLIDER_CLOSE

        leftOuttakeSlider.targetPosition = outTakePosition
        leftOuttakeSlider.mode = DcMotor.RunMode.RUN_TO_POSITION
        leftOuttakeSlider.power = 1.0

        rightOuttakeSlider.targetPosition = outTakePosition
        rightOuttakeSlider.mode = DcMotor.RunMode.RUN_TO_POSITION
        rightOuttakeSlider.power = 1.0
    }

    fun stop() {
        leftOuttakeSlider.power = 0.0
        rightOuttakeSlider.power = 0.0
    }

    fun placeStone(af1:  Boolean, af2: Boolean) {
        if(af1 == true) {
            backLeftServo.position = backLeftServoDown
            backRightServo.position = backRightServoDown
            frontLeftServo.position = frontLeftServoDown
            frontRightServo.position = frontRightServoDown
        } else if(af2 == true) {
            backLeftServo.position = backLeftServoOpen
            backRightServo.position = backRightServoOpen
            frontLeftServo.position = frontLeftServoOpen
            frontRightServo.position = frontRightServoOpen
        } else {
            backLeftServo.position = backLeftServoClosed
            backRightServo.position = backRightServoClosed
            frontLeftServo.position = frontLeftServoClosed
            frontRightServo.position = frontRightServoClosed
        }
    }


    fun grabStone(af : Boolean) {
        if(af == true) {
            grabServo.position = grabServoOpen
        } else {
            grabServo.position = grabServoClosed
        }
    }

    fun printPosition(telemetry: Telemetry) {
        telemetry.addLine("Position outtake")
                .addData("Left", "%d", leftOuttakeSlider.currentPosition)
                .addData("Right", "%d", rightOuttakeSlider.currentPosition)
    }

    fun setIndividualPower (S: String, v: Double){
        if (S.toLowerCase() == "left"){
//            leftOuttakeSlider.mode = DcMotor.RunMode.RUN_USING_ENCODER
            leftOuttakeSlider.power = v
        }
        else{
//            rightOuttakeSlider.mode = DcMotor.RunMode.RUN_USING_ENCODER
            rightOuttakeSlider.power = v
        }
    }

    fun setPowers(v1: Double, v2: Double) {
        leftOuttakeSlider.power = v1
        rightOuttakeSlider.power = v2
    }

    fun setServoPositions(backLeft : Double, backRight : Double, frontLeft : Double, frontRight : Double, grab : Double, cap : Double) {
        backLeftServo.position = backLeft
        backRightServo.position = backRight
        frontLeftServo.position = frontLeft
        frontRightServo.position = frontRight
        grabServo.position = grab
        capServo.position = cap
    }

    fun capStone(af: Boolean){
        if (!af){
            capServo.position = capServoOpen
        }
        else{
            capServo.position = capServoClose
        }
    }
}
