package org.firstinspires.ftc.teamcode.autonomy

import android.view.VelocityTracker
import com.acmerobotics.dashboard.config.Config
import com.acmerobotics.roadrunner.geometry.Pose2d
import com.acmerobotics.roadrunner.geometry.Vector2d
import com.acmerobotics.roadrunner.path.heading.HeadingInterpolator
import com.qualcomm.robotcore.eventloop.opmode.Autonomous
import com.qualcomm.robotcore.hardware.DcMotor
import org.firstinspires.ftc.teamcode.OpMode
import org.firstinspires.ftc.teamcode.RoadRunner.drive.mecanum.SampleMecanumDriveREVOptimized
import org.firstinspires.ftc.teamcode.hardware.Hardware
import org.firstinspires.ftc.teamcode.waitMillis
import java.util.*

@Autonomous
@Config
class Autonomyspecialrosu : OpMode() {
    val drive by lazy {
        SampleMecanumDriveREVOptimized(hardwareMap)
    }

    override fun Hardware.run() {
        motors.frontRightMotor.mode = DcMotor.RunMode.STOP_AND_RESET_ENCODER
        while(motors.frontRightMotor.currentPosition < 1000) {
            motors.translate(-90.0, -0.4)
        }
        waitMillis(15000)

        motors.frontRightMotor.mode = DcMotor.RunMode.STOP_AND_RESET_ENCODER
        while(motors.frontRightMotor.currentPosition > -1000) {
            motors.translate(0.0, -0.4)
        }
    }
}