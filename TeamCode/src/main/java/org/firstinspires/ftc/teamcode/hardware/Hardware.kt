package org.firstinspires.ftc.teamcode.hardware

import com.qualcomm.robotcore.hardware.HardwareMap
import org.firstinspires.ftc.teamcode.hardware.sensors.DistanceColorSensor
import org.firstinspires.ftc.teamcode.hardware.sensors.Imu
import org.firstinspires.ftc.teamcode.hardware.sensors.REVDistanceSensor

class Hardware(hwMap: HardwareMap) {
    val motors = DriveMotors(hwMap)
    val intake = Intake(hwMap)
    val outTake = Outtake(hwMap)
    val imu = Imu(hwMap)
    val foundation = Foundation(hwMap)
//    val distanceSensor = REVDistanceSensor(hwMap)
//    val colorSensor = DistanceColorSensor(hwMap)

    fun stop() {
        motors.stop()
        intake.stop()
        outTake.stop()
        foundation.stop()
    }
}
