package org.firstinspires.ftc.teamcode

import com.qualcomm.robotcore.eventloop.opmode.Disabled
import com.qualcomm.robotcore.eventloop.opmode.TeleOp
import com.qualcomm.robotcore.util.ElapsedTime
import org.firstinspires.ftc.teamcode.hardware.Hardware
import org.firstinspires.ftc.teamcode.hardware.Intake
import java.lang.Math.atan2
import kotlin.math.absoluteValue

@TeleOp(name = "CompleteDrivee", group = "Main")
class CompleteDrivee: OpMode() {

    override fun preInit(){
    }

    override fun preInitLoop() {
        telemetry.addLine("Waiting for start...")
        telemetry.update()
        idle()
    }

    override fun Hardware.run() {
        val gp1 = Gamepad(gamepad1)
        val gp2 = Gamepad(gamepad2)

        var isGrabbing = false
        var isPlacing = false
        var isPlacingDown = false
        var isCapping = false
        var moveFoundation = false
        var isSlow = false


        while(opModeIsActive()) {

            if(gp1.checkToggle(Gamepad.Button.Y)) isSlow = !isSlow

            val power = when(isSlow) {
                true -> 0.3 * speed
                else -> speed
            }

            val rotPower = when(isSlow) {
                true -> 0.5 * rotation
                else -> rotation
            }


            intake.setIntakePower(gp2.left_stick_y.toDouble(), gp2.right_stick_y.toDouble())
            hw.motors.move(direction, power, rotPower)
            outTake.moveSlider((gp2.right_trigger - gp2.left_trigger).toDouble())

            if(gp2.checkToggle(Gamepad.Button.A)) isGrabbing = !isGrabbing
            if(gp2.right_bumper) outTake.grabServo.position = 1.0
            else outTake.grabStone(isGrabbing)
            if(gp2.checkToggle(Gamepad.Button.X)) isPlacingDown = !isPlacingDown
            if(gp2.checkToggle(Gamepad.Button.B)) isPlacing = !isPlacing
            outTake.placeStone(isPlacingDown, isPlacing)

            if(gp2.checkToggle(Gamepad.Button.Y)) moveFoundation = !moveFoundation
            foundation.grab(moveFoundation)

            if(gp2.checkToggle(Gamepad.Button.START)) isCapping = !isCapping
            outTake.capStone(isCapping)

            telemetry.addData("Outake target", outTake.outTakePosition)
            outTake.printPosition(telemetry)
            telemetry.addData("IsGrabbing", isGrabbing)
            telemetry.update()
        }
    }

    /// The direction in which the robot is translating.
    private val direction: Double
        get() {
            val x = gamepad1.left_stick_x.toDouble()
            val y = -gamepad1.left_stick_y.toDouble()

            return atan2(y, x) / Math.PI * 180.0 - 90.0
        }

    /// Rotation around the robot's Z axis.
    private val rotation: Double
        get() = -gamepad1.right_stick_x.toDouble()

    /// Translation speed.
    private val speed: Double
        get() {
            val x = gamepad1.left_stick_x.toDouble()
            val y = gamepad1.left_stick_y.toDouble()

            return Math.sqrt((x * x) + (y * y))
        }
}
